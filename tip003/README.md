# 手順めも

### 0. Prerequisites
```
sudo apt update
sudo apt install libwebkit2gtk-4.0-dev \
    build-essential \
    curl \
    wget \
    file \
    libssl-dev \
    libgtk-3-dev \
    libayatana-appindicator3-dev \
    librsvg2-dev
```

## A. Quick start

### 1. create-tauri-app
```
cargo install create-tauri-app --locked
cargo create-tauri-app
```

#### cargo create-tauri-app
```
gitpod /workspace/tauri (main) $ cargo create-tauri-app
✔ Project name · tip001a
✔ Choose which language to use for your frontend · TypeScript / JavaScript - (pnpm, yarn, npm)
✔ Choose your package manager · npm
✔ Choose your UI template · Vanilla
✔ Choose your UI flavor · JavaScript

Template created! To get started run:
  cd tip001a
  npm install
  npm run tauri dev

gitpod /workspace/tauri (main) $ ll
total 20
drwxr-xr-x 7 gitpod gitpod  137 Sep 17 04:19 ./
drwxr-xr-x 7 gitpod gitpod   91 Sep 17 04:09 ../
drwxr-xr-x 8 gitpod gitpod 4096 Sep 17 04:19 .git/
-rw-r--r-- 1 gitpod gitpod  216 Sep 17 04:09 .gitpod.Dockerfile
-rw-r--r-- 1 gitpod gitpod  105 Sep 17 04:09 .gitpod.yml
-rw-r--r-- 1 gitpod gitpod 6172 Sep 17 04:09 README.md
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip001/
drwxr-xr-x 5 gitpod gitpod  104 Sep 17 04:19 tip001a/
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip002/
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip003/
gitpod /workspace/tauri (main) $ 
gitpod /workspace/tauri (main) $ tree tip001a
tip001a
├── package.json
├── README.md
├── src
│   ├── assets
│   │   ├── javascript.svg
│   │   └── tauri.svg
│   ├── index.html
│   ├── main.js
│   └── styles.css
└── src-tauri
    ├── build.rs
    ├── Cargo.toml
    ├── icons
    │   ├── 128x128@2x.png
    │   ├── 128x128.png
    │   ├── 32x32.png
    │   ├── icon.icns
    │   ├── icon.ico
    │   ├── icon.png
    │   ├── Square107x107Logo.png
    │   ├── Square142x142Logo.png
    │   ├── Square150x150Logo.png
    │   ├── Square284x284Logo.png
    │   ├── Square30x30Logo.png
    │   ├── Square310x310Logo.png
    │   ├── Square44x44Logo.png
    │   ├── Square71x71Logo.png
    │   ├── Square89x89Logo.png
    │   └── StoreLogo.png
    ├── src
    │   └── main.rs
    └── tauri.conf.json

5 directories, 27 files
gitpod /workspace/tauri (main) $ 
```

### 2. npm
```
cd tip001a
npm install
npm run tauri dev
```

#### npm install
```
gitpod /workspace/tauri/tip001a (main) $ ll
total 12
drwxr-xr-x 5 gitpod gitpod 104 Sep 17 04:19 ./
drwxr-xr-x 7 gitpod gitpod 137 Sep 17 04:19 ../
-rw-r--r-- 1 gitpod gitpod 253 Sep 17 04:19 .gitignore
-rw-r--r-- 1 gitpod gitpod 186 Sep 17 04:19 package.json
-rw-r--r-- 1 gitpod gitpod 373 Sep 17 04:19 README.md
drwxr-xr-x 3 gitpod gitpod  71 Sep 17 04:19 src/
drwxr-xr-x 4 gitpod gitpod 105 Sep 17 04:19 src-tauri/
drwxr-xr-x 2 gitpod gitpod  29 Sep 17 04:19 .vscode/
gitpod /workspace/tauri/tip001a (main) $ cat -n package.json
     1  {
     2    "name": "tip001a",
     3    "private": true,
     4    "version": "0.0.0",
     5    "type": "module",
     6    "scripts": {
     7      "tauri": "tauri"
     8    },
     9    "devDependencies": {
    10      "@tauri-apps/cli": "^1.4.0"
    11    }
    12  }
gitpod /workspace/tauri/tip001a (main) $ 
gitpod /workspace/tauri/tip001a (main) $ npm install

added 3 packages, and audited 4 packages in 2s

1 package is looking for funding
  run `npm fund` for details

found 0 vulnerabilities
npm notice 
npm notice New major version of npm available! 9.6.7 -> 10.1.0
npm notice Changelog: https://github.com/npm/cli/releases/tag/v10.1.0
npm notice Run npm install -g npm@10.1.0 to update!
npm notice 
gitpod /workspace/tauri/tip001a (main) $ echo $?
0
gitpod /workspace/tauri/tip001a (main) $ ll
total 20
drwxr-xr-x 6 gitpod gitpod  149 Sep 17 04:23 ./
drwxr-xr-x 7 gitpod gitpod  137 Sep 17 04:19 ../
-rw-r--r-- 1 gitpod gitpod  253 Sep 17 04:19 .gitignore
drwxr-xr-x 4 gitpod gitpod   63 Sep 17 04:23 node_modules/
-rw-r--r-- 1 gitpod gitpod  186 Sep 17 04:19 package.json
-rw-r--r-- 1 gitpod gitpod 6179 Sep 17 04:23 package-lock.json
-rw-r--r-- 1 gitpod gitpod  373 Sep 17 04:19 README.md
drwxr-xr-x 3 gitpod gitpod   71 Sep 17 04:19 src/
drwxr-xr-x 4 gitpod gitpod  105 Sep 17 04:19 src-tauri/
drwxr-xr-x 2 gitpod gitpod   29 Sep 17 04:19 .vscode/
gitpod /workspace/tauri/tip001a (main) $ 
```

#### npm run tauri dev
```
gitpod /workspace/tauri/tip001a (main) $ npm run tauri dev

...(omit)...

   Compiling tauri-macros v1.4.0
   Compiling tip001a v0.0.0 (/workspace/tauri/tip001a/src-tauri)
   Compiling cairo-rs v0.15.12
   Compiling pango v0.15.10
   Compiling atk v0.15.1
   Compiling javascriptcore-rs v0.16.0
   Compiling gdk-pixbuf v0.15.11
   Compiling soup2 v0.2.1
   Compiling gdk v0.15.4
   Compiling webkit2gtk v0.18.2
    Finished dev [unoptimized + debuginfo] target(s) in 2m 23s

(tip001a:9264): dbind-WARNING **: 04:29:56.766: AT-SPI: Error retrieving accessibility bus address: org.freedesktop.DBus.Error.ServiceUnknown: The name org.a11y.Bus was not provided by any .service files
Could not determine the accessibility bus address
```

### 3.
PORTSタブで6080のURLを開く  

### 参考：構成
```
gitpod /workspace/tauri (main) $ ll
total 20
drwxr-xr-x 7 gitpod gitpod  137 Sep 17 04:19 ./
drwxr-xr-x 7 gitpod gitpod   91 Sep 17 04:09 ../
drwxr-xr-x 8 gitpod gitpod 4096 Sep 17 04:35 .git/
-rw-r--r-- 1 gitpod gitpod  216 Sep 17 04:09 .gitpod.Dockerfile
-rw-r--r-- 1 gitpod gitpod  105 Sep 17 04:09 .gitpod.yml
-rw-r--r-- 1 gitpod gitpod 6172 Sep 17 04:09 README.md
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip001/
drwxr-xr-x 6 gitpod gitpod  149 Sep 17 04:23 tip001a/
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip002/
drwxr-xr-x 2 gitpod gitpod   59 Sep 17 04:09 tip003/
gitpod /workspace/tauri (main) $ tree tip001a -L 4
tip001a
├── node_modules
│   └── @tauri-apps
│       ├── cli
│       │   ├── build.rs
│       │   ├── Cargo.toml
│       │   ├── CHANGELOG.md
│       │   ├── index.d.ts
│       │   ├── index.js
│       │   ├── jest.config.js
│       │   ├── LICENSE_APACHE-2.0
│       │   ├── LICENSE_MIT
│       │   ├── main.d.ts
│       │   ├── main.js
│       │   ├── package.json
│       │   ├── README.md
│       │   ├── schema.json
│       │   ├── src
│       │   └── tauri.js
│       ├── cli-linux-x64-gnu
│       │   ├── cli.linux-x64-gnu.node
│       │   ├── package.json
│       │   └── README.md
│       └── cli-linux-x64-musl
│           ├── cli.linux-x64-musl.node
│           ├── package.json
│           └── README.md
├── package.json
├── package-lock.json
├── README.md
├── src
│   ├── assets
│   │   ├── javascript.svg
│   │   └── tauri.svg
│   ├── index.html
│   ├── main.js
│   └── styles.css
└── src-tauri
    ├── build.rs
    ├── Cargo.lock
    ├── Cargo.toml
    ├── icons
    │   ├── 128x128@2x.png
    │   ├── 128x128.png
    │   ├── 32x32.png
    │   ├── icon.icns
    │   ├── icon.ico
    │   ├── icon.png
    │   ├── Square107x107Logo.png
    │   ├── Square142x142Logo.png
    │   ├── Square150x150Logo.png
    │   ├── Square284x284Logo.png
    │   ├── Square30x30Logo.png
    │   ├── Square310x310Logo.png
    │   ├── Square44x44Logo.png
    │   ├── Square71x71Logo.png
    │   ├── Square89x89Logo.png
    │   └── StoreLogo.png
    ├── src
    │   └── main.rs
    ├── target
    │   ├── CACHEDIR.TAG
    │   └── debug
    │       ├── build
    │       ├── deps
    │       ├── examples
    │       ├── incremental
    │       ├── tip001a
    │       └── tip001a.d
    └── tauri.conf.json

17 directories, 52 files
gitpod /workspace/tauri (main) $ 
```
