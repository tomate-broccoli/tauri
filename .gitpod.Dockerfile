FROM gitpod/workspace-full-vnc

USER gitpod

RUN sudo apt-get update && \
    sudo apt-get install -y libgtk-3-dev && \ 
    sudo apt-get install -y libgl1-mesa-dev xorg-dev && \
    sudo rm -rf /var/lib/apt/lists/*

# for tauri-cli
RUN sudo apt update && \
    sudo apt install -y \
        libwebkit2gtk-4.0-dev \
        build-essential \
        curl \
        wget \
        file \
        libssl-dev \
        libgtk-3-dev \
        libayatana-appindicator3-dev \
        librsvg2-dev

# CMD ["cargo", "install", "tauri-cli" ]
RUN cargo install tauri-cli

RUN cargo install create-tauri-app
