//
const { invoke } = window.__TAURI__.tauri;

window.addEventListener("DOMContentLoaded", () => {
    document.querySelector("#btn").addEventListener("click", (e) => {
        console.log('*** click: ', e)
        // alert('#btn clicked!')
        const res = invoke("my_click", { msg: "#btn clicked!!"});
        console.log('** res:', res)
    });
});
