# 手順めも

### 0. Prerequisites
```
sudo apt update
sudo apt install libwebkit2gtk-4.0-dev \
    build-essential \
    curl \
    wget \
    file \
    libssl-dev \
    libgtk-3-dev \
    libayatana-appindicator3-dev \
    librsvg2-dev
```

## A. Quick start

### 1. create-tauri-app
```
cargo install create-tauri-app --locked
cargo create-tauri-app
```

#### cargo create-tauri-app
```
gitpod /workspace/tauri/tip001 (main) $ cargo create-tauri-app
✔ Project name · tip001a
✔ Choose which language to use for your frontend · TypeScript / JavaScript - (pnpm, yarn, npm)
✔ Choose your package manager · npm
✔ Choose your UI template · Vanilla
✔ Choose your UI flavor · JavaScript

Template created! To get started run:
  cd tip001a
  npm install
  npm run tauri dev

gitpod /workspace/tauri/tip001 (main) $ 
```

### 2. npm
```
cd tip001a
npm install
```

```
gitpod /workspace/tauri/tip001a/src (main) $ ll
total 16
drwxr-xr-x 3 gitpod gitpod   93 Sep 16 06:08 ./
drwxr-xr-x 6 gitpod gitpod  149 Sep 16 06:02 ../
drwxr-xr-x 2 gitpod gitpod   45 Sep 16 06:01 assets/
-rw-r--r-- 1 gitpod gitpod 1224 Sep 16 06:08 index.html
-rw-r--r-- 1 gitpod gitpod 1218 Sep 16 06:01 index.html.ORG
-rw-r--r-- 1 gitpod gitpod  557 Sep 16 06:01 main.js
-rw-r--r-- 1 gitpod gitpod 1615 Sep 16 06:01 styles.css
gitpod /workspace/tauri/tip001a/src (main) $ cat -n index.html
     1  <!doctype html>
     2  <html lang="en">
     3    <head>
     4      <meta charset="UTF-8" />
     5      <link rel="stylesheet" href="styles.css" />
     6      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     7      <title>Tauri App</title>
     8      <script type="module" src="/main.js" defer></script>
     9      <style>
    10        .logo.vanilla:hover {
    11          filter: drop-shadow(0 0 2em #ffe21c);
    12        }
    13      </style>
    14    </head>
    15
    16    <body>
    17      <div class="container">
    18        <h1>ようこそTauriへ！</h1>
    19
    20        <div class="row">
    21          <a href="https://tauri.app" target="_blank">
    22            <img src="/assets/tauri.svg" class="logo tauri" alt="Tauri logo" />
    23          </a>
    24          <a
    25            href="https://developer.mozilla.org/en-US/docs/Web/JavaScript"
    26            target="_blank"
    27          >
    28            <img
    29              src="/assets/javascript.svg"
    30              class="logo vanilla"
    31              alt="JavaScript logo"
    32            />
    33          </a>
    34        </div>
    35
    36        <p>Click on the Tauri logo to learn more about the framework</p>
    37
    38        <form class="row" id="greet-form">
    39          <input id="greet-input" placeholder="Enter a name..." />
    40          <button type="submit">Greet</button>
    41        </form>
    42
    43        <p id="greet-msg"></p>
    44      </div>
    45    </body>
    46  </html>
gitpod /workspace/tauri/tip001a/src (main) $ 
```

```
npm run tauri dev
```

### 3.
PORTSタブで6080のURLを開く  
[![tip_001a.png](./tip_001a.png)]

