# 手順めも

### 0. Prerequisites
```
sudo apt update
sudo apt install libwebkit2gtk-4.0-dev \
    build-essential \
    curl \
    wget \
    file \
    libssl-dev \
    libgtk-3-dev \
    libayatana-appindicator3-dev \
    librsvg2-dev
```

## B. tauri-cli
```
cargo install tauri-cli
mkdir tip002a
cd tip002a
cargo tauri init
cargo tauri dev
```

#### cargo tauri init
```
gitpod /workspace/tauri/tip002a (main) $ cargo tauri init
✔ What is your app name? · tip002a
✔ What should the window title be? · tip002a
✔ Where are your web assets (HTML/CSS/JS) located, relative to the "<current dir>/src-tauri/tauri.conf.json" file that will be created? · ../ui
✔ What is the url of your dev server? · ../ui
✔ What is your frontend dev command? · echo 'npm run dev'
✔ What is your frontend build command? · echo 'npm run build'
gitpod /workspace/tauri/tip002a (main) $ ll
total 0
drwxr-xr-x 3 gitpod gitpod  23 Sep 17 05:22 ./
drwxr-xr-x 7 gitpod gitpod 137 Sep 17 05:21 ../
drwxr-xr-x 4 gitpod gitpod 105 Sep 17 05:22 src-tauri/
gitpod /workspace/tauri/tip002a (main) $ 
itpod /workspace/tauri/tip002a (main) $ tree src-tauri
src-tauri
├── build.rs
├── Cargo.toml
├── icons
│   ├── 128x128@2x.png
│   ├── 128x128.png
│   ├── 32x32.png
│   ├── icon.icns
│   ├── icon.ico
│   ├── icon.png
│   ├── Square107x107Logo.png
│   ├── Square142x142Logo.png
│   ├── Square150x150Logo.png
│   ├── Square284x284Logo.png
│   ├── Square30x30Logo.png
│   ├── Square310x310Logo.png
│   ├── Square44x44Logo.png
│   ├── Square71x71Logo.png
│   ├── Square89x89Logo.png
│   └── StoreLogo.png
├── src
│   └── main.rs
└── tauri.conf.json

2 directories, 20 files
gitpod /workspace/tauri/tip002a (main) $ 
gitpod /workspace/tauri/tip002a (main) $ cat -n src-tauri/tauri.conf.json
     1  {
     2    "build": {
     3      "beforeBuildCommand": "echo 'npm run build'",
     4      "beforeDevCommand": "echo 'npm run dev'",
     5      "devPath": "../ui",
     6      "distDir": "../ui"
     7    },
     8    "package": {
     9      "productName": "tip002a",
    10      "version": "0.1.0"
    11    },
    12    "tauri": {
    13      "allowlist": {
    14        "all": false
    15      },
    16      "bundle": {
    17        "active": true,
    18        "category": "DeveloperTool",
    19        "copyright": "",
    20        "deb": {
    21          "depends": []
    22        },
    23        "externalBin": [],
    24        "icon": [
    25          "icons/32x32.png",
    26          "icons/128x128.png",
    27          "icons/128x128@2x.png",
    28          "icons/icon.icns",
    29          "icons/icon.ico"
    30        ],
    31        "identifier": "com.tauri.dev",
    32        "longDescription": "",
    33        "macOS": {
    34          "entitlements": null,
    35          "exceptionDomain": "",
    36          "frameworks": [],
    37          "providerShortName": null,
    38          "signingIdentity": null
    39        },
    40        "resources": [],
    41        "shortDescription": "",
    42        "targets": "all",
    43        "windows": {
    44          "certificateThumbprint": null,
    45          "digestAlgorithm": "sha256",
    46          "timestampUrl": ""
    47        }
    48      },
    49      "security": {
    50        "csp": null
    51      },
    52      "updater": {
    53        "active": false
    54      },
    55      "windows": [
    56        {
    57          "fullscreen": false,
    58          "height": 600,
    59          "resizable": true,
    60          "title": "tip002a",
    61          "width": 800
    62        }
    63      ]
    64    }
    65  }
gitpod /workspace/tauri/tip002a (main) $ 
```

#### cargo tauri dev
```
gitpod /workspace/tauri/tip002a (main) $ ll
total 0
drwxr-xr-x 3 gitpod gitpod  23 Sep 17 04:53 ./
drwxr-xr-x 8 gitpod gitpod 152 Sep 17 04:44 ../
drwxr-xr-x 4 gitpod gitpod 105 Sep 17 04:53 src-tauri/
gitpod /workspace/tauri/tip002a (main) $ mkdir ui
gitpod /workspace/tauri/tip002a (main) $ touch ui/index.html
gitpod /workspace/tauri/tip002a (main) $ cargo tauri dev

...(omit)...

   Compiling tauri-macros v1.4.0
   Compiling app v0.1.0 (/workspace/tauri/tip002a/src-tauri)
   Compiling pango v0.15.10
   Compiling cairo-rs v0.15.12
   Compiling atk v0.15.1
   Compiling javascriptcore-rs v0.16.0
   Compiling gdk-pixbuf v0.15.11
   Compiling soup2 v0.2.1
   Compiling gdk v0.15.4
   Compiling webkit2gtk v0.18.2
    Finished dev [unoptimized + debuginfo] target(s) in 2m 16s

(tip002a:12910): dbind-WARNING **: 05:29:00.629: AT-SPI: Error retrieving accessibility bus address: org.freedesktop.DBus.Error.ServiceUnknown: The name org.a11y.Bus was not provided by any .service files
Could not determine the accessibility bus address
```

### 5.
PORTSタブで6080のURLを開く  
