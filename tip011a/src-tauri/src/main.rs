use tauri::Manager;

fn main() {
    tauri::Builder::default()
        // .invoke_handler(tauri::generate_handler![my_click])
        .setup(|app| {
            // listen to the `event-name` (emitted on any window)
            let _id = app.listen_global("click", |event| {
                println!("got event-name with payload {:?}", event.payload());
            });
            // unlisten to the event using the `id` returned on the `listen_global` function
            // a `once_global` API is also exposed on the `App` struct
            // app.unlisten(id);
            Ok(())
         })
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
