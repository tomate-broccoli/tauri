//
const { emit } = window.__TAURI__.event;

window.addEventListener("DOMContentLoaded", () => {
    document.querySelector("#btn").addEventListener("click", (e) => {
        console.log('*** click: ', e)
        // alert('#btn clicked!')
        // invoke("my_click", { msg: "#btn clicked!!"});
	emit('click', { theMessage: 'Tauri is awesome!', })
    });
});
